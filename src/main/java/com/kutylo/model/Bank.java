package com.kutylo.model;

import java.time.LocalDate;
import java.util.EnumSet;

public class Bank {
    private int bankId;
    private String name;
    private String country;
    private EnumSet<DepositType> depositTypes;
    private String depositor;
    private int accountId;
    private int amountOnDeposit;
    private int profitability;
    private LocalDate timeConstrains;

    public Bank(){}

    public Bank(int bankId, String name, String country, EnumSet<DepositType> depositTypes, String depositor, int accountId, int amountOnDeposit, int profitability, LocalDate timeConstrains) {
        this.bankId = bankId;
        this.name = name;
        this.country = country;
        this.depositTypes = depositTypes;
        this.depositor = depositor;
        this.accountId = accountId;
        this.amountOnDeposit = amountOnDeposit;
        this.profitability = profitability;
        this.timeConstrains = timeConstrains;
    }

    //    public <E extends Enum<E>> Bank(int i, String bank, String country, EnumSet<E> value, String depositor, int i1, int nextInt, int i2, LocalDate plusYears) {
//    }

    public int getBankId() {
        return bankId;
    }

    public void setBankId(int bankId) {
        this.bankId = bankId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public EnumSet<DepositType> getDepositTypes() {
        return depositTypes;
    }

    public void setDepositTypes(EnumSet<DepositType> depositTypes) {
        this.depositTypes = depositTypes;
    }

    public String getDepositor() {
        return depositor;
    }

    public void setDepositor(String depositor) {
        this.depositor = depositor;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getAmountOnDeposit() {
        return amountOnDeposit;
    }

    public void setAmountOnDeposit(int amountOnDeposit) {
        this.amountOnDeposit = amountOnDeposit;
    }

    public int getProfitability() {
        return profitability;
    }

    public void setProfitability(int profitability) {
        this.profitability = profitability;
    }

    public LocalDate getTimeConstrains() {
        return timeConstrains;
    }

    public void setTimeConstrains(LocalDate timeConstrains) {
        this.timeConstrains = timeConstrains;
    }

    @Override
    public String toString() {
        return "Bank{" +
                "bankId=" + bankId +
                ", country='" + country + '\'' +
                ", depositTypes=" + depositTypes +
                ", depositor='" + depositor + '\'' +
                ", accountId=" + accountId +
                ", amountOnDeposit=" + amountOnDeposit +
                ", profitability=" + profitability +
                ", timeConstrains=" + timeConstrains +
                '}';
    }
}
