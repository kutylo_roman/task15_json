package com.kutylo;

import com.kutylo.jsonParser.GSONParser;
import com.kutylo.jsonParser.JSONValidator;
import com.kutylo.jsonParser.JacksonParser;
import com.kutylo.jsonParser.Parser;
import com.kutylo.model.Bank;
import com.kutylo.model.DepositType;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Random;

public class Application {
    public String json = "src/main/resources/json/bank.json";
    public String schema = "src/main/resources/json/bankSchema.json";
    public List<Bank> list;

    public Application(String json, String schema, List<Bank> list) {
        this.json = json;
        this.schema = schema;
        this.list = list;
    }

    @SuppressWarnings("unchecked")
    private void parse(Parser parser) {
        System.out.println("Saved: " + list);
        parser.writeJSON(list);
        System.out.println("Retrieved" + parser.readJSON());
    }

    public void jackson() {  //TODO
        JacksonParser jacksonParser = new JacksonParser(new File(json));
        parse(jacksonParser);
    }

    public void gson() {
        GSONParser gsonParser = new GSONParser(new File(json));
        parse(gsonParser);
    }

    public void validate() {
        try {
            System.out.println(new JSONValidator().isValid(new File(json), new File(schema)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static List<Bank> generate(int number) {
        List<Bank> banks = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < number; i++) {
            int temp;
            banks.add(new Bank(i, "Bank", "Country",
                    EnumSet.of(
                            DepositType.values()[
                                    random.nextInt(DepositType.values().length)]),
                    "Depositor",
                    i,
                    random.nextInt(1000),
                    1,
                    LocalDate.now().plusYears(2 + random.nextInt(10))));
        }
        return banks;
    }

    public static void main(String[] args) throws IOException {
        Application app1 = new Application("src/main/resources/json/bank1.json", "src/main/resources/json/bankSchema.json", Application.generate(5));
        Application app2 = new Application("src/main/resources/json/bank2.json", "src/main/resources/json/bankSchema.json", Application.generate(5));
        app1.jackson();
        app2.gson();
    }
}
